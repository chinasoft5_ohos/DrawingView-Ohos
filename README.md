# DrawingView-Ohos

#### 项目介绍
- 项目名称：DrawingView-Ohos
- 所属系列：openharmony的第三方组件适配移植
- 功能：DrawingView是一个简单的视图，允许您使用手指在屏幕上绘制，并将图形保存为图像。
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：Release 1.0.13 

#### 效果演示


#### 安装教程

1.在项目根目录下的build.gradle文件中，
 ```
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
 ```

2.在entry模块的build.gradle文件中，

 ```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:DrawingView-Ohos:1.0.0')
    ......  
 }
 ```
在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明
1.添加DrawingView到布局当中
 ```
   <com.mukesh.DrawingView
            ohos:id="$+id:scratch_pad"
            ohos:width="match_parent"
            ohos:height="match_parent"
            ohos:above="$+id:buttons"/>
 ```
2.在ability中获取控件,使用其中的方法:
 ```
    Component componentById = findComponentById(ResourceTable.Id_scratch_pad);
        if (componentById instanceof DrawingView) {
            drawingView = (DrawingView) componentById;
        }
 drawingView.initializePen(); //使用笔模式,在屏幕上绘图
 drawingView.initializeEraser(); //使用橡皮擦模式,清除绘制
 drawingView.setBackgroundColor(color); //设置画布背景色
 drawingView.setEraserSize(size);//设置橡皮擦的size
 drawingView.setPenSize(size);//设置画笔的size
 drawingView.setPenColor(color);//设置画笔的颜色
 drawingView.saveImage(getExternalFilesDir(Environment.DIRECTORY_PICTURES).getPath(), "test.png", 100);//保存图片
 drawingView.loadImage(getPixelMapFromResource(getContext(), ResourceTable.Media_image));//加载图片
 drawingView.clear(); //清除画布上的内容
 ```
#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.0
- 0.0.1-SNAPSHOT

#### 版权和许可信息
```
MIT License

Copyright (c) 2018 Mukesh Solanki

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
