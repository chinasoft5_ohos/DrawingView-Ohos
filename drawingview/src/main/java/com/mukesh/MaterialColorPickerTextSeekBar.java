/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mukesh;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Slider;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;
import ohos.hiviewdfx.HiLogLabel;

import static ohos.hiviewdfx.HiLog.LOG_APP;

/**
 * Created by Patrick Geselbracht on 2017-03-04
 */
class MaterialColorPickerTextSeekBar extends Slider implements Component.DrawTask {
    private Paint textPaint;
    private Rect textRect;
    private int textColor;
    private int textSize;
    private String text;

    /**
     * 构造方法
     *
     * @param context context
     */
    public MaterialColorPickerTextSeekBar(Context context) {
        super(context);
    }

    /**
     * 构造方法
     *
     * @param context context
     * @param attrSet attrSet
     */
    public MaterialColorPickerTextSeekBar(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(attrSet);
    }

    /**
     * 构造方法
     *
     * @param context   context
     * @param attrSet   attrSet
     * @param styleName styleName
     */
    public MaterialColorPickerTextSeekBar(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(attrSet);
    }

    private void init(AttrSet attrs) {
        textPaint = new Paint();
        textRect = new Rect();
        textColor = AttrUtils.getColorFromAttr(attrs, "progress_hint_text_color", getProgressHintTextColor().getValue());
        textSize = AttrUtils.getDimensionFromAttr(attrs, "textSize", 40);
        text = AttrUtils.getStringFromAttr(attrs, "text", null);

        textPaint.setColor(new Color(textColor));
        textPaint.setTextSize(textSize);
        textPaint.setTextAlign(TextAlignment.CENTER);
        textPaint.setStyle(Paint.Style.FILL_STYLE);
        textRect = textPaint.getTextBounds("255");
        setPadding(getPaddingLeft(), (int) (0.8 * textRect.getHeight()),
                getPaddingRight(), getPaddingBottom());

        addDrawTask(this);

    }

    /**
     * onDraw
     *
     * @param component component
     * @param canvas    canvas
     */
    @Override
    public void onDraw(Component component, Canvas canvas) {
        int progress = getProgress();
        Rect bounds = getThumbElement().getBounds();
        float xAxis = (float) ((progress / 255.0 * getProgressElement().getWidth()) + getPaddingLeft() + 10 + (bounds.getWidth() >> 2));
        canvas.drawText(textPaint,
                (text == null) ? String.valueOf(getProgress()) : text,
                xAxis,
                textRect.getHeight() + (getPaddingTop() >> 2)

        );


    }
}
