/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */
package com.mukesh;

import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import ohos.agp.utils.TextTool;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 自定义属性工具类
 * <p>
 * 注意： 宽高都为 match_content 且无实际内容时构造方法不会调用
 * 使用方法：
 * xxx extends Component
 * 获取自定义属性：
 * String count = AttrUtils.getStringFromAttr(attrSet,"cus_count","0");
 * <p>
 * 属性定义：
 * 布局头中加入  xmlns:hap="http://schemas.huawei.com/apk/res/ohos" 使用hap区分自定义属性与系统属性
 * 即可使用 hap:cus_count="2"  不加直接使用ohos:cus_count="2"
 *
 *
 * <p>
 */
public class AttrUtils {
    private static final String TAG = "RippleView";
    private static HiLogLabel sLogLabel;
    private static final int DOMAIN = 0x001;

    /**
     * getStringFromAttr
     *
     * @param attrSet      attrSet
     * @param name         name
     * @param defaultValue defaultValue
     * @return String
     */
    public static String getStringFromAttr(AttrSet attrSet, String name, String defaultValue) {
        String value = defaultValue;
        if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
            value = attrSet.getAttr(name).get().getStringValue();
        }
        return value;
    }

    /**
     * getDimensionFromAttr
     *
     * @param attrSet      attrSet
     * @param name         name
     * @param defaultValue defaultValue
     * @return Integer
     */
    public static Integer getDimensionFromAttr(AttrSet attrSet, String name, Integer defaultValue) {
        Integer value = defaultValue;
        if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
            value = attrSet.getAttr(name).get().getDimensionValue();
        }
        return value;
    }

    /**
     * getIntegerFromAttr
     *
     * @param attrSet      attrSet
     * @param name         name
     * @param defaultValue defaultValue
     * @return Integer
     */
    public static Integer getIntegerFromAttr(AttrSet attrSet, String name, Integer defaultValue) {
        Integer value = defaultValue;
        if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
            value = attrSet.getAttr(name).get().getIntegerValue();
        }
        return value;
    }

    /**
     * getFloatFromAttr
     *
     * @param attrSet      attrSet
     * @param name         name
     * @param defaultValue defaultValue
     * @return float
     */
    public static float getFloatFromAttr(AttrSet attrSet, String name, float defaultValue) {
        float value = defaultValue;
        if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
            value = attrSet.getAttr(name).get().getFloatValue();
        }

        return value;
    }

    /**
     * getLongFromAttr
     *
     * @param attrSet      attrSet
     * @param name         name
     * @param defaultValue defaultValue
     * @return Long
     */
    public static Long getLongFromAttr(AttrSet attrSet, String name, Long defaultValue) {
        Long value = defaultValue;
        if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
            value = attrSet.getAttr(name).get().getLongValue();
        }

        return value;
    }

    /**
     * getColorFromAttr
     *
     * @param attrSet      attrSet
     * @param name         name
     * @param defaultValue defaultValue
     * @return int
     */
    public static int getColorFromAttr(AttrSet attrSet, String name, int defaultValue) {
        int value = defaultValue;
        if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
            value = attrSet.getAttr(name).get().getColorValue().getValue();

        }
        return value;
    }

    /**
     * getBooleanFromAttr
     *
     * @param attrSet      attrSet
     * @param name         name
     * @param defaultValue defaultValue
     * @return boolean
     */
    public static boolean getBooleanFromAttr(AttrSet attrSet, String name, boolean defaultValue) {
        boolean value = defaultValue;
        if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
            value = attrSet.getAttr(name).get().getBoolValue();
        }

        return value;
    }

    /**
     * getElementFromAttr
     *
     * @param attrSet        attrSet
     * @param name           name
     * @param defaultElement defaultElement
     * @return Element
     */
    public static Element getElementFromAttr(AttrSet attrSet, String name, Element defaultElement) {
        Element element = defaultElement;
        if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
            element = attrSet.getAttr(name).get().getElement();
        }

        return element;
    }

    private static HiLogLabel createLogLabel() {
        if (sLogLabel == null) {
            sLogLabel = new HiLogLabel(HiLog.LOG_APP, DOMAIN, TAG);
        }
        return sLogLabel;
    }

    /**
     * getLayoutIdFormAttr
     *
     * @param attrSet attrSet
     * @param name    name
     * @param def     def
     * @return int
     */
    public static int getLayoutIdFormAttr(AttrSet attrSet, String name, int def) {
        int id = def;
        String idStr = getStringFromAttr(attrSet, name, "");
        if (!TextTool.isNullOrEmpty(idStr) && idStr.contains(":")) {
            id = Integer.parseInt(idStr.substring(idStr.indexOf(':') + 1));
        }
        LogUtil.loge("AttrUtils.getLayoutIdFormAttr=" + idStr + "/" + id);
        return id;
    }

    /**
     * error
     *
     * @param format  format
     * @param objects objects
     */
    public static void error(String format, Object... objects) {
        HiLog.error(createLogLabel(), format, objects);
    }

    /**
     * debug
     *
     * @param format  format
     * @param objects objects
     */
    public static void debug(String format, Object... objects) {
        HiLog.debug(createLogLabel(), format, objects);
    }

    /**
     * getMax
     *
     * @param radius radius
     * @return int
     */
    public static int getMax(int... radius) {
        if (radius.length == 0) {
            return 0;
        }
        int max = radius[0];
        for (int item : radius) {
            if (item > max) {
                max = item;
            }
        }
        return max;
    }

    /**
     * red
     *
     * @param color color
     * @return int
     */
    public static int red(int color) {
        return (color >> 16) & 0xFF;
    }

    /**
     * green
     *
     * @param color color
     * @return int
     */
    public static int green(int color) {
        return (color >> 8) & 0xFF;
    }

    /**
     * blue
     *
     * @param color color
     * @return int
     */
    public static int blue(int color) {
        return color & 0xFF;
    }


    /**
     * invokeStringValue
     *
     * @param property     property
     * @param defaultValue defaultValue
     * @return String
     */
    public static String invokeStringValue(String property, String defaultValue) {
        String value = defaultValue;
        try {
            Class clazz = Class.forName("android.os.SystemProperties");
            Method method = clazz.getMethod("get", String.class, String.class);
            value = (String) method.invoke(null, property, defaultValue);
        } catch (ClassNotFoundException classnotfoundexception) {
            error("occured ClassNotFoundException");
        } catch (NoSuchMethodException nosuchmethodexception) {
            error("occured NoSuchMethodException");
        } catch (IllegalAccessException e) {
            error("occured invoke");
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return value;
    }


}
