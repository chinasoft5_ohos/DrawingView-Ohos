/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mukesh;

import ohos.agp.render.Paint;
import ohos.agp.render.Path;

/**
 * PaintPathBean
 */
public class PaintPathBean {
    private Path path;
    private Paint paint;

    /**
     * getPath
     *
     * @return Path
     */
    public Path getPath() {
        return path;
    }

    /**
     * setPath
     *
     * @param path path
     */
    public void setPath(Path path) {
        this.path = path;
    }

    /**
     * getPaint
     *
     * @return Paint
     */
    public Paint getPaint() {
        return paint;
    }

    /**
     * setPaint
     *
     * @param paint paint
     */
    public void setPaint(Paint paint) {
        this.paint = paint;
    }

    /**
     * 构造方法
     *
     * @param path  path
     * @param paint paint
     */
    public PaintPathBean(Path path, Paint paint) {
        this.path = path;
        this.paint = paint;
    }
}
