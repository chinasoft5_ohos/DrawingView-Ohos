package com.mukesh.drawingview.example.slice;

import com.mukesh.*;
import com.mukesh.drawingview.example.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Slider;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.app.Context;
import ohos.app.Environment;
import ohos.global.resource.NotExistException;
import ohos.global.resource.Resource;
import ohos.global.resource.WrongTypeException;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainAbilitySlice extends AbilitySlice
        implements Component.ClickedListener, Slider.ValueChangedListener {
    private Button saveButton;
    private Button penButton;
    private Button eraserButton;
    private Button penColorButton;
    private Button backgroundColorButton;
    private Button loadButton;
    private Button clearButton;
    private DrawingView drawingView;
    private Slider penSizeSeekBar;
    private Slider eraserSizeSeekBar;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
//        getWindow().setInputPanelDisplayType(WindowManager.LayoutConfig.INPUT_ADJUST_PAN);
        Color statusBar = getColorFromRes(
                this, ResourceTable.Color_colorPrimaryDark);
        getWindow().setStatusBarColor(statusBar.getValue());
        initializeUI();
        setListeners();
        String s = ColorFormatHelper.formatColorValues(100,100, 100, 100);
        Logger.getLogger("TAG").log(Level.INFO,"获取到的值为==="+s);
        final ColorPicker colorPicker = new ColorPicker(
                this,
                255,
                127,
                123,
                67
        );
        colorPicker.setAlignment(LayoutAlignment.CENTER);
        colorPicker.setTransparent(true);
        colorPicker.enableAutoClose();
        colorPicker.setAll(202, 127, 45, 99);
    }

    public static Color getColorFromRes(Context context, int res) {
        Color color = null;
        try {
            color = new Color(context.getResourceManager().getElement(res).getColor());
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
        return color;
    }

    private void setListeners() {
        saveButton.setClickedListener(this);
        penButton.setClickedListener(this);
        eraserButton.setClickedListener(this);
        penColorButton.setClickedListener(this);
        backgroundColorButton.setClickedListener(this);
        penSizeSeekBar.setValueChangedListener(this);
        eraserSizeSeekBar.setValueChangedListener(this);
        loadButton.setClickedListener(this);
        clearButton.setClickedListener(this);
    }

    private void initializeUI() {
        Component componentById = findComponentById(ResourceTable.Id_scratch_pad);
        if (componentById instanceof DrawingView) {
            drawingView = (DrawingView) componentById;
        }
        if (componentById instanceof Button) {
            saveButton = (Button) componentById;
            loadButton = (Button) componentById;
            penButton = (Button) componentById;
            eraserButton = (Button) componentById;
            penColorButton = (Button) componentById;
            backgroundColorButton = (Button) componentById;
            clearButton = (Button) componentById;
        }
        if (componentById instanceof Slider) {
            penSizeSeekBar = (Slider) componentById;
            eraserSizeSeekBar = (Slider) componentById;
        }
        drawingView = (DrawingView) findComponentById(ResourceTable.Id_scratch_pad);
        saveButton = (Button) findComponentById(ResourceTable.Id_save_button);
        loadButton = (Button) findComponentById(ResourceTable.Id_load_button);
        penButton = (Button) findComponentById(ResourceTable.Id_pen_button);
        eraserButton = (Button) findComponentById(ResourceTable.Id_eraser_button);
        penColorButton = (Button) findComponentById(ResourceTable.Id_pen_color_button);
        backgroundColorButton = (Button) findComponentById(ResourceTable.Id_background_color_button);
        penSizeSeekBar = (Slider) findComponentById(ResourceTable.Id_pen_size_seekbar);
        eraserSizeSeekBar = (Slider) findComponentById(ResourceTable.Id_eraser_size_seekbar);
        clearButton = (Button) findComponentById(ResourceTable.Id_clear_button);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_save_button:
                String test ="";
                        try {
                            test = getResourceManager().getElement(ResourceTable.String_test).getString();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (NotExistException e) {
                            e.printStackTrace();
                        }catch (WrongTypeException e) {
                            e.printStackTrace();
                        }
                if(test!=null&&!test.equals("")){
                    drawingView.saveImage(getExternalFilesDir(Environment.DIRECTORY_PICTURES).getPath(), test, 100);
                }

                break;
            case ResourceTable.Id_load_button:
                drawingView.loadImage(getPixelMapFromResource(getContext(), ResourceTable.Media_image));
                break;
            case ResourceTable.Id_pen_button:
                drawingView.initializePen();
                break;
            case ResourceTable.Id_eraser_button:
                drawingView.initializeEraser();
                break;
            case ResourceTable.Id_clear_button:
                drawingView.clear();
                break;
            case ResourceTable.Id_pen_color_button:

                final ColorPicker colorPicker = new ColorPicker(MainAbilitySlice.this, 100, 100, 100);
                colorPicker.setCallback(
                        new ColorPickerCallback() {
                            @Override
                            public void onColorChosen(int color) {
                                drawingView.setPenColor(color);
                            }
                        });
                colorPicker.enableAutoClose();
                colorPicker.setAlignment(LayoutAlignment.CENTER);
                colorPicker.setAutoClosable(true);
                colorPicker.show();
                break;
            case ResourceTable.Id_background_color_button:
                final ColorPicker backgroundColorPicker = new ColorPicker(MainAbilitySlice.this, 100, 100, 100);
                backgroundColorPicker.setCallback(
                        new ColorPickerCallback() {
                            @Override
                            public void onColorChosen(int color) {
                                LogUtil.loge("=======>" + color);
                                drawingView.setBackgroundColor(color);
                            }
                        });
                backgroundColorPicker.setAlignment(LayoutAlignment.CENTER);
                backgroundColorPicker.enableAutoClose();
                backgroundColorPicker.setAutoClosable(true);
                backgroundColorPicker.show();
                break;
            default:
                break;
        }
    }

    @Override
    public void onProgressUpdated(Slider slider, int i, boolean b) {
        int SliderId = slider.getId();
        if (SliderId == ResourceTable.Id_pen_size_seekbar) {
            drawingView.setPenSize(i);
        } else {
            drawingView.setEraserSize(i);
        }
    }

    @Override
    public void onTouchStart(Slider slider) {
    }

    @Override
    public void onTouchEnd(Slider slider) {
    }

    private PixelMap getPixelMapFromResource(Context context, int imgRes) {
        byte[] imgData = null;
        try {
            Resource imgResource = context.getResourceManager().getResource(imgRes);
            int imgLength = imgResource.available();
            imgData = new byte[imgLength];
            imgResource.read(imgData);
        } catch (IOException | NotExistException e) {
            e.printStackTrace();
        }
        if (imgData == null) {
            return null;
        }
        ImageSource imageSource = ImageSource.create(imgData, null);
        return imageSource.createPixelmap(null);
    }
}
